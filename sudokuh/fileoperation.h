#ifndef _FILEOPERATION_H_
#define _FILEOPERATION_H_

#include <stdio.h>
#include "board.h"

board* spawnBoardFromFile(char* filename);
void safeBoardToFile(board* b,char* filename);


#endif
