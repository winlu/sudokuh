#include <stdio.h>
#include <stdlib.h>

#include "board.h"
#include "group.h"
#include "field.h"

#include "fileoperation.h"

void printLine(){
    printf("------------------------\n");
}

void printLine2(){
    printLine();
    printLine();
}

void test_set(){
    board* tmp = spawn_board();

    field_setSolution_u(tmp->groups[4]->fields[4],10);
    board_print_guesscount(tmp);
    field_setSolution(tmp->groups[4]->fields[5],2);

    printLine();
    field_debug(tmp->groups[4]->fields[4]);
    field_debug(tmp->groups[4]->fields[5]);
    printLine();
    field_setSolution(tmp->fields[4][4],2);
    board_print(tmp);
}

void test_guess(){
    unsigned int i,j,a;

    a=511;

    i=solutionToGuess(5);
    j=solutionToGuess(7);

    a = (a-i);
    a = (a-j);

    printf("i:%u\nj:%u\na:%u\n\n",i,j,a);
}

void test_new(){

    int temp = guessToSolution(1);
    printf("%d\n",temp);

    temp = guessToSolution(8);
    printf("%d\n",temp);

    temp = guessToSolution(16);
    printf("%d\n",temp);

    temp = solutionToGuess(9);
    printf("%u\n",temp);

    temp = solutionToGuess(7);
    printf("%u\n",temp);
}

void testgroups(){
        int i=0;
        board* tmp = spawn_board();
        for(i=0;i<fieldsize;i++){
            group_debug(tmp->groups[i]);
            system("pause");
        }
}

void work_with_files(){
     board* tmp = spawnBoardFromFile("testfiles/in.0");
     board_print(tmp);
     printLine();
     field_setSolution(tmp->groups[4]->fields[4],2);
     board_print(tmp);
     safeBoardToFile(tmp,"testfiles/out.0");
}

void test_auto_fill(){
     board* tmp = spawnBoardFromFile("testfiles/in.1");
     board_print(tmp);
     printLine();
     safeBoardToFile(tmp,"testfiles/out.1");
}

void test_solve(){
    board* medium0 = spawnBoardFromFile("testfiles/medium/in.0");


    printf("medium0:\n");
    board_print(medium0);
    printLine();
    board_print_guesscount(medium0);
    printLine();
    /*
    board* hard0 = spawnBoardFromFile("testfiles/hard/in.0");

    printf("hard0:\n");
    board_print(hard0);
    board_print_guesscount(hard0);
    */
}

int main()
{
    /*
    test_new();
    printLine2();
    */

    /*
    test_set();
    printLine2();
    */
    //testgroups();


    //work_with_files();

    //test_auto_fill();

    test_solve();
    return 0;
}
