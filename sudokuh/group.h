#ifndef _GROUP_H_
#define _GROUP_H_

#include "field.h"

void group_init(group* g);
group* spawn_group();

void group_addField(group* g, field* f);

void group_debug(group* g);
void group_printLine(group* g, int l);
void group_printColumn(group* g, int c);
void group_printLine_count(group* g, int l);

void group_removeGuess_u(group* g, unsigned int u);
void group_removeGuess(group* g, int i);


#endif
