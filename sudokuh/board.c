#include <stdio.h>
#include <stdlib.h>

#include "board.h"

void board_init(board* b){
    int line=0;
    int colu=0;
    int groupnr=-1;

    /*spawn groups, link board to groups*/
    for(line=0;line<fieldsize;line++){
        b->groups[line]=spawn_group();
        b->groups[line]->board_p=b;
    }

    /*spawn fields + link board to fields + link fields to board +link fields to group + link group to fields*/
    for(line=0;line<fieldsize;line++){
        for(colu=0;colu<fieldsize;colu++){
            groupnr = board_getGroup_standard(line,colu);

            b->fields[line][colu]=spawn_field(line,colu);
            b->fields[line][colu]->group_p=b->groups[groupnr];
            b->fields[line][colu]->board_p=b;
            group_addField(b->groups[groupnr],b->fields[line][colu]);
        }
    }
}


board* spawn_board(){
    board* tmp = malloc(sizeof(board));
    board_init(tmp);
    return tmp;
}

void board_print(board* b){
    int line=0;
    int colu=0;

    for(line=0;line<fieldsize;line++){
        for(colu=0;colu<fieldsize;colu++){
            field_printSolution(b->fields[line][colu]);
            if(colu==2)printf(" ");
            else if(colu==5)printf(" ");
        }
        if(line==2)printf("\n");
        else if(line==5)printf("\n");
        printf("\n");
    }
    printf("\n");

    /*
    cool but old

    int i=0,j=0,startpos=0;

    for(;startpos<7;startpos+=3){
        for(i=0;i<3;i++){
            for(j=startpos;j<(startpos+3);j++){
                group_printLine(b->groups[j],i);
                printf(" ");
            }
            printf("\n");
        }
        printf("\n");
    }
    */
}

void board_print_guesscount(board* b){
    int line=0;
    int colu=0;

    for(line=0;line<fieldsize;line++){
        for(colu=0;colu<fieldsize;colu++){
            field_printGuessCount(b->fields[line][colu]);
            if(colu==2)printf(" ");
            else if(colu==5)printf(" ");
        }
        if(line==2)printf("\n");
        else if(line==5)printf("\n");
        printf("\n");
    }
}

void board_removeGuess_line_column(board* b, unsigned int guess , int line, int col){
    int i=0;
    for(i=0;i<fieldsize;i++){
        field_removeGuess(b->fields[i][col], guess);
    }
    for(i=0;i<fieldsize;i++){
        field_removeGuess(b->fields[line][i], guess);
    }
}

int board_getGroup_standard(int line, int colu){
    int groupcounter=0;

    if(line<3){
        groupcounter+=0;
    }else if(line<6){
        groupcounter+=3;
    }else if(line<9){
        groupcounter+=6;
    }
    if(colu<3){
        groupcounter+=0;
    }else if(colu<6){
        groupcounter+=1;
    }else if(colu<9){
        groupcounter+=2;
    }
    return groupcounter;
}
