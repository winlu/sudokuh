#ifndef _FIELD_H_
#define _FIELD_H_

#include "structures.h"

void field_init(field* f);
field* spawn_field(int line, int column);

void field_debug(field* f);
void field_printSolution(field* f);
void field_printGuessCount(field* f);

void field_setSolution_u(field* f, unsigned int u);
void field_setSolution(field* f, int i);

int field_getSolution(field* f);

int guessToSolution(unsigned int u);
unsigned int solutionToGuess(int i);

void field_removeGuess(field* f, unsigned int u);

/*
int field_toColumn(field* f);
int field_toLine(field* f);
*/
#endif
