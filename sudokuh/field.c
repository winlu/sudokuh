#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "field.h"
#include "group.h"
#include "board.h"

void field_init(field* f){
    f->guess=511;
    f->field_line=0;
    f->field_column=0;
    f->guess_count=fieldsize;
    f->group_p=NULL;
    f->board_p=NULL;
}

field* spawn_field(int line, int column){
    field* tmp = malloc(sizeof(field));
    field_init(tmp);
    tmp->field_line=line;
    tmp->field_column=column;
    return tmp;
}

void field_debug(field* f){

    printf("line:%d\n"
           "column:%d\n"
           "guess_count:%d\n"
           "guess:%u\n"
           "guessconverted:%d\n",f->field_line , f->field_column ,f->guess_count, f->guess, guessToSolution(f->guess));

    printf("\n");
}

void field_printSolution(field* f){
    if(f->guess_count==0){
        printf("%d", guessToSolution(f->guess));
    }else{
        printf("0");
    }
}

void field_printGuessCount(field* f){
    printf("%d", f->guess_count);
}

void field_setSolution_u(field* f, unsigned int u){
    if((f->guess&u)>0){
        f->guess=u;
        f->guess_count=0;
        group_removeGuess_u(f->group_p,u);
        board_removeGuess_line_column(((group*)f->group_p)->board_p, u, f->field_line, f->field_column);
    }else{
        printf("WARNING: TRYING TO SET %d BUT NOT ALLOWED AT %d,%d!\n", guessToSolution(u), f->field_line, f->field_column);
    }
}

void field_setSolution(field* f, int i){
    field_setSolution_u(f,solutionToGuess(i));
}

int field_getSolution(field* f){
    if(f->guess_count>0)return 0;
    return guessToSolution(f->guess);
}



int guessToSolution(unsigned int u){
    int i=0;

    for(;i<fieldsize;i++){
         if((u&(1<<i))>0){
            return i+1;
        }
    }
    return 0;
}

unsigned int solutionToGuess(int i){
    return 1<<(i-1);
}

void field_removeGuess(field* f,unsigned int u){
    if(((f->guess&u)>0)&&(f->guess_count>0)){
        f->guess_count=f->guess_count-1;
        f->guess=f->guess-u;

        if(f->guess_count==1){
            printf("AUTO:only guess %d left for %d,%d...Setting\n", guessToSolution(f->guess), f->field_line,f->field_column);
            field_setSolution_u(f,f->guess);
        }
    }else{
        /*
        **only for debug
        **printf("WARNING: TRYING TO remove %d from guess list but already removed AT %d,%d!\n", guessToSolution(u), f->field_line, f->field_column);
        */
    }


}

/*
int field_toColumn(field* f){
    return (((group*)f->group_p)->group_location%3)*3+(f->field_location%3);
}

int field_toLine(field* f){
    int solution=0;
    int group_loc=((group*)f->group_p)->group_location;
    int field_loc=f->field_location;

    if(group_loc<3){
        solution+=0;
    }else if(group_loc<6){
        solution+=3;
    }else if(group_loc<9){
        solution+=6;
    }

    if(field_loc<3){
        solution+=0;
    }else if(field_loc<6){
        solution+=1;
    }else if(field_loc<9){
        solution+=2;
    }

    return solution;
}
*/

