#include <stdio.h>
#include <stdlib.h>

#include "fileoperation.h"

board* spawnBoardFromFile(char* filename){
    int line=0,colu=0;
    int tempchar=0;

    FILE* in = fopen(filename,"r");

    if (in == NULL) {
        fprintf(stderr, "Can't open input file %s!\n", filename);
        exit(1);
    }

    board* tmp = spawn_board();
    board_init(tmp);

    while(1){
        tempchar=fgetc(in);
        if(tempchar==-1){
            printf("EOF not expected, abort setting");
            fclose(in);
            exit(3);
        }
        if(tempchar<48||tempchar>57){
            /*printf("found bad sign at %d,%d:%d\n",line,colu,tempchar);*/
            continue;
        }
        tempchar-=48;
        if(tempchar!=0){
            field_setSolution(tmp->fields[line][colu],tempchar);
        }
        colu++;
        if(colu==fieldsize){
            line++;
            colu=0;
        }
        if(line>=fieldsize)break;
    }

    fclose(in);
    return tmp;
}

void safeBoardToFile(board* b, char* filename){
    int line=0;
    int colu=0;

    FILE* out = fopen(filename,"w");

    if (out == NULL) {
        fprintf(stderr, "Can't open output file %s!\n", filename);
        exit(1);
    }

    for(line=0;line<fieldsize;line++){
        for(colu=0;colu<fieldsize;colu++){
            fputc(field_getSolution(b->fields[line][colu])+48, out);
            if(colu==2||colu==5){
                fputc(32, out);
            }
        }
        fputc(10, out);
        if(line==2||line==5){
            fputc(10, out);
        }
    }
    fclose(out);
}
