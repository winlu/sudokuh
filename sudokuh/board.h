#ifndef _BOARD_H_
#define _BOARD_H_

#include "group.h"

void board_init(board* b);
board* spawn_board();

void board_print(board* b);
void board_print_guesscount(board* b);

void board_removeGuess_line_column(board* b, unsigned int guess, int line, int col);

int board_getGroup_standard(int line, int colu);
#endif

