#include <stdio.h>
#include <stdlib.h>

#include "group.h"

void group_init(group* g){
    int i=0;

    for(;i<fieldsize;i++){
        g->fields[i]=NULL;
    }
    g->field_count=0;
    g->board_p=NULL;
}

group* spawn_group(){
    group* tmp = malloc(sizeof(group));
    group_init(tmp);
    return tmp;
}

void group_addField(group* g, field* f){
    g->fields[g->field_count++]=f;
}

void group_debug(group* g){
    int i=0;
    for(;i<fieldsize;i++){
        field_debug(g->fields[i]);
        printf("\n");
    }
    printf("\n");
}

void group_printLine(group* g, int l){
    int i;
    for(i=0;i<3;i++){
        field_printSolution(g->fields[l*3+i]);
    }
}

void group_printColumn(group* g, int c){
    int i;
    for(i=0;i<3;i++){
        field_printSolution(g->fields[c+i*3]);
        printf("\n");
    }
}

void group_printLine_count(group* g, int l){
    int i;
    for(i=0;i<3;i++){
        field_printGuessCount(g->fields[l*3+i]);
    }
}

void group_removeGuess_u(group* g, unsigned int u){
    int i=0;

    for(;i<fieldsize;i++){
        field_removeGuess(g->fields[i], u);
    }
}

void group_removeGuess(group* g, int i){
    group_removeGuess_u(g,solutionToGuess(i));
}
