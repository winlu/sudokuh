#ifndef _STRUCTURES_H_
#define _STRUCTURES_H_

#define fieldsize 9

typedef struct field_s{
    unsigned int guess;
    int guess_count;
    int field_line;
    int field_column;

    void* group_p;
    void* board_p;

}field;

typedef struct group_s{
    field* fields[fieldsize];
    int field_count;

    void* board_p;
}group;

typedef struct board_s{
    group* groups[fieldsize];
    field* fields[fieldsize][fieldsize];
}board;

#endif
